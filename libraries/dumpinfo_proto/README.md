# dumpinfo_proto
DumpInfo Protobuf helper library for MFRC522 reader

**Designed to be used with Arduino Uno**

## Dependencies
Depends on:
* [dumpinfo_serialio](https://gitlab.com/mtyszczak/dumpinfo/-/blob/main/libraries/dumpinfo_serialio)

## clang-format
Use file from this repository in order to format your C++ code in a proper way:
[mtyszczak/clang-format-file](https://gitlab.com/mtyszczak/clang-format-file)

## License
See in [LICENSE.md](https://gitlab.com/mtyszczak/dumpinfo/-/blob/main/libraries/dumpinfo_proto/LICENSE.md)
