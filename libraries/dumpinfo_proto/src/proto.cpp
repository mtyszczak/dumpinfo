#include "proto.hpp"

namespace dumpinfo { namespace proto { namespace __detail {
  void print_byte_with( const byte buf, serialio::output& stream )
  {
    const auto old_base = stream.get_base();
    if( buf < 0x10 )
      stream << "0";
    stream << serialio::hex << buf << old_base;
  }

  /**
   * Helper routine to dump a byte array as hex values to Serial.
   */
  void dump_bytes_with( const byte* buffer, size_t size, serialio::output& stream )
  {
    if( !size )
      return;

    const byte* const end = buffer + size;

    print_byte_with( *buffer, stream );

    while( ++buffer != end )
    {
      stream << " ";
      print_byte_with( *buffer, stream );
    }
  }
}}} // namespace dumpinfo::proto::__detail
