#pragma once

#include <Arduino.h>

#include <serialio.hpp>

namespace dumpinfo { namespace proto {
  namespace __detail {
    void print_byte_with( const byte buf, serialio::output& stream );
    void dump_bytes_with( const byte* buffer, size_t size, serialio::output& stream );
  } // namespace __detail

  template< typename T >
  class span {
  public:
    span( T* buffer, size_t size ) : buffer( buffer ), size( size ) {}
    ~span() = default;

    void print_with( serialio::output& stream ) const { __detail::dump_bytes_with( this->buffer, this->size, stream ); }

    T* buffer;
    size_t size;
  };

}} // namespace dumpinfo::proto

template< typename T >
dumpinfo::serialio::output& operator<<( dumpinfo::serialio::output& stream, const dumpinfo::proto::span< T >& value )
{
  value.print_with( stream );
  return stream;
}
