#include "serialio.hpp"

namespace dumpinfo { namespace serialio {

  constexpr __detail::setprecision_type setprecision( uint8_t value )
  {
    return __detail::setprecision_type{ .value = value };
  }

  output::output( Print& print ) : print( print ) {}

  __detail::base_type output::get_base() const
  {
    return this->base;
  }
  __detail::setprecision_type output::get_precision() const
  {
    return this->precision;
  }

  output& output::operator<<( __detail::setprecision_type precision )
  {
    this->precision.value = precision.value;
    return *this;
  }

  output& output::operator<<( __detail::base_type base )
  {
    this->base.value = base.value;
    return *this;
  }

  output& output::operator<<( __detail::endl_type )
  {
    print.println();
    print.flush();
    return *this;
  }

  /*
    output& output::operator<<( bool value )
    {
      print.print( value ? "true" : "false" );
      return *this;
    }
  */

  output& output::operator<<( uint8_t value )
  {
    print.print( value, this->base.value );
    return *this;
  }
  output& output::operator<<( int8_t value )
  {
    print.print( value, this->base.value );
    return *this;
  }

  output& output::operator<<( uint16_t value )
  {
    print.print( value, this->base.value );
    return *this;
  }
  output& output::operator<<( int16_t value )
  {
    print.print( value, this->base.value );
    return *this;
  }

  output& output::operator<<( uint32_t value )
  {
    print.print( value, this->base.value );
    return *this;
  }
  output& output::operator<<( int32_t value )
  {
    print.print( value, this->base.value );
    return *this;
  }

  output& output::operator<<( uint64_t value )
  {
    print.print( static_cast< long unsigned int >( value ), this->base.value );
    return *this;
  }
  output& output::operator<<( int64_t value )
  {
    print.print( static_cast< long int >( value ), this->base.value );
    return *this;
  }

  output& output::operator<<( float value )
  {
    print.print( value, this->precision.value );
    return *this;
  }
  output& output::operator<<( double value )
  {
    print.print( value, this->precision.value );
    return *this;
  }

  output& output::operator<<( const char* value )
  {
    print.print( value );
    return *this;
  }
  output& output::operator<<( const String& value )
  {
    print.print( value );
    return *this;
  }

}} // namespace dumpinfo::serialio
