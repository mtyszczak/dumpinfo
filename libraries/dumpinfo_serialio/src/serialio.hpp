#pragma once

#include <Arduino.h>

namespace dumpinfo { namespace serialio {
  class output;

  namespace __detail {
    struct base_type
    {
      ::uint8_t value;
    };

    struct setprecision_type
    {
      ::uint8_t value;
    };

    class endl_type {};
  } // namespace __detail

  static constexpr __detail::base_type bin = { .value = BIN };
  static constexpr __detail::base_type oct = { .value = OCT };
  static constexpr __detail::base_type dec = { .value = DEC };
  static constexpr __detail::base_type hex = { .value = HEX };

  static constexpr __detail::endl_type endl; // flushes

  constexpr __detail::setprecision_type setprecision( uint8_t value );

  class output {
  private:
    __detail::base_type base              = dec;
    __detail::setprecision_type precision = { .value = 5 };
    Print& print;

  public:
    output( Print& print = Serial );
    ~output() = default;

    __detail::base_type get_base() const;
    __detail::setprecision_type get_precision() const;

    output& operator<<( __detail::setprecision_type precision );
    output& operator<<( __detail::base_type base );
    output& operator<<( __detail::endl_type );
    /*
        output& operator<<( bool value );
    */
    output& operator<<( uint8_t value );
    output& operator<<( int8_t value );

    output& operator<<( uint16_t value );
    output& operator<<( int16_t value );

    output& operator<<( uint32_t value );
    output& operator<<( int32_t value );

    output& operator<<( uint64_t value );
    output& operator<<( int64_t value );

    output& operator<<( float value );
    output& operator<<( double value );

    output& operator<<( const char* value );
    output& operator<<( const String& value );
  };

  static output out;
}} // namespace dumpinfo::serialio
