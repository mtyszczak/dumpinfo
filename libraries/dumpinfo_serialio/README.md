# dumpinfo_serialio
DumpInfo Serial input/output helper library similar to the std::cout

**Designed to be used with Arduino Uno**

## clang-format
Use file from this repository in order to format your C++ code in a proper way:
[mtyszczak/clang-format-file](https://gitlab.com/mtyszczak/clang-format-file)

## License
See in [LICENSE.md](https://gitlab.com/mtyszczak/dumpinfo/-/blob/main/libraries/dumpinfo_serialio/LICENSE.md)
