# dumpinfo
Simple fake RFID banking system

**Designed to be used with Arduino Uno and RFID-RC522 RFID card reader**

Developer note: This software was created just for fun :D

## Pinout
See [src/DumpInfo.ino](src/DumpInfo.ino)

## Build
0. Install plocate, avr-libc, clang-format-15, git, arduino, code, `git submodule update --init --recursive --progress`
1. Install arduino-cli
2. `arduino-cli config init`
3. `arduino-cli board list`
4. `arduino-cli core install arduino:avr`
5. Install arduino vsc extension

## Verify and upload
Click `Ctrl`+`Shift`+`P` and then type: `Arduino: Verify` and then `Arduino: Upload`

Before uploading the `scripts/prebuild.sh` script will be executed which searches for the Arduino library directory
using `locate` command and then creates there symbolic links to all of the project libraries.

## clang-format
Use file from this repository in order to format your C++ code in a proper way:
[mtyszczak/clang-format-file](https://gitlab.com/mtyszczak/clang-format-file)

## License
See in [LICENSE.md](LICENSE.md)
