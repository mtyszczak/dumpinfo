/*
 * Typical pin layout used:
 * ------------------------------------
 *             MFRC522      Arduino
 *             Reader/PCD   Uno/101
 * Signal      Pin          Pin
 * ------------------------------------
 * RST/Reset   RST          RST
 * SPI SS      SDA(SS)      10
 * SPI MOSI    MOSI         11 / ICSP-4
 * SPI MISO    MISO         12 / ICSP-1
 * SPI SCK     SCK          13 / ICSP-3
 */

#include <Arduino.h>
#include <SPI.h>

#include <MFRC522Constants.h>
#include <MFRC522Debug.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522DriverSPI.h>
#include <MFRC522v2.h>

#include <proto.hpp>
#include <serialio.hpp>

#define SS_PIN 10 // Configurable, see typical pin layout above

MFRC522DriverPinSimple ss_pin{ SS_PIN }; // Configurable, see typical pin layout above.

MFRC522DriverSPI driver{ ss_pin }; // Create SPI driver.
MFRC522 mfrc522{ driver };         // Create MFRC522 instance.

MFRC522::MIFARE_Key key = {
  .keyByte = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff}
};

// MFRC522::MIFARE_Key key_a = { .keyByte = {0x00, 0xde, 0xad, 0xc0, 0xde, 0x0a} };
// MFRC522::MIFARE_Key key_b = { .keyByte = {0x00, 0xde, 0xad, 0xc0, 0xde, 0x0b} }; // TODO: Add custom keys

namespace dp = dumpinfo::proto;
namespace ds = dumpinfo::serialio;

using cbyte_span = dp::span< const byte >;

void setup()
{
  Serial.begin( 9600 );                                     // Initialize serial communications with the PC
  SPI.begin();                                              // Init SPI bus
  mfrc522.PCD_Init();                                       // Init MFRC522
  MFRC522Debug::PCD_DumpVersionToSerial( mfrc522, Serial ); // Show details of PCD - MFRC522 Card Reader details

  ds::out << "PCD Serial " << mfrc522.PCD_GetVersion() << ds::endl
          << "Using key (for A): " << cbyte_span{ key.keyByte, MFRC522Constants::MIFARE_Misc::MF_KEY_SIZE } << ds::endl
          << "BEWARE: Data will be written to the PICC, in sector #1" << ds::endl
          << ds::endl;
}

bool auth_a()
{
  // Authenticate using key A
  ds::out << "Authenticating using key A...";

  byte trailerBlock = 7;

  MFRC522::StatusCode status = static_cast< MFRC522::StatusCode >(
    mfrc522.PCD_Authenticate( MFRC522Constants::PICC_Command::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &( mfrc522.uid ) ) );

  if( status != MFRC522Constants::StatusCode::STATUS_OK )
    ds::out << " Failed: " << MFRC522Debug::GetStatusCodeName( status ) << ds::endl;
  else
    ds::out << " Done." << ds::endl;

  return status == MFRC522Constants::StatusCode::STATUS_OK;
}

bool write_to( byte blockAddr )
{
  if( !auth_a() )
    return false;

  // Write data to the block
  ds::out << "Writing data to block " << blockAddr << "...";

  // Using little endian:
  //     manufacturer id,    4-byte user id     ,  7-byte username terminated with \0  ,      (free 4-bytes)
  byte buffer[] = { 0x00, 0xde, 0xad, 0xc0, 0xde, 'b', 'o', 'b', '\0', '\0', '\0', '\0', 0x10, 0x00, 0x00, 0x00 };
  // free last 4-bytes can be used for example as an optional account balance for the offline usage
  byte size = sizeof( buffer ) / sizeof( buffer[0] );

  MFRC522::StatusCode status = mfrc522.MIFARE_Write( blockAddr, buffer, size );
  if( status != MFRC522Constants::StatusCode::STATUS_OK )
    ds::out << " Failed: " << MFRC522Debug::GetStatusCodeName( status ) << ds::endl;
  else
    ds::out << " Done." << ds::endl;

  return status == MFRC522Constants::StatusCode::STATUS_OK;
}

bool read_from( byte blockAddr )
{
  // Read data from the block
  ds::out << "Reading data from block " << blockAddr << "...";
  byte buffer[18];
  byte size                  = sizeof( buffer ) / sizeof( buffer[0] );
  MFRC522::StatusCode status = (MFRC522::StatusCode) mfrc522.MIFARE_Read( blockAddr, buffer, &size );
  if( status != MFRC522Constants::StatusCode::STATUS_OK )
    ds::out << " Failed: " << MFRC522Debug::GetStatusCodeName( status ) << ds::endl;
  else
    ds::out << " Done." << ds::endl << "  Data in block " << blockAddr << ": " << cbyte_span{ buffer, 16 } << ds::endl;

  return status == MFRC522Constants::StatusCode::STATUS_OK;
}

void read_and_write()
{
  byte blockAddr = 4;

  if( !write_to( blockAddr ) )
    return;

  if( !read_from( blockAddr ) )
    return;
}

void print_card_info()
{
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType( mfrc522.uid.sak );

  ds::out << "Card UID:  " << cbyte_span{ mfrc522.uid.uidByte, mfrc522.uid.size } << ds::endl
          << "PICC type: " << MFRC522Debug::PICC_GetTypeName( mfrc522.PICC_GetType( piccType ) ) << ds::endl;
}

void loop()
{
  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if( !mfrc522.PICC_IsNewCardPresent() )
    return;

  // Select one of the cards
  if( !mfrc522.PICC_ReadCardSerial() )
    return;

  // Show some details of the PICC (that is: the tag/card)
  print_card_info();

  read_and_write();

  ds::out << ds::endl;

  // Halt PICC
  mfrc522.PICC_HaltA();
  // Stop encryption on PCD
  mfrc522.PCD_StopCrypto1();
}
