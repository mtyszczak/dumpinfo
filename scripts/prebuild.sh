#!/bin/sh

PRG="$0"
# need this for relative symlinks
while [ -h "$PRG" ]; do
   PRG=$(readlink "$PRG")
done

SELF_DIR=$(realpath $(dirname "$PRG"))

# Base project configuration
PROJECT_NAME="DumpInfo"
PROJECT_ROOT_DIR=$(dirname "${SELF_DIR}")

# Project sub-directories and important files
PROJECT_SCRIPTS_DIR="${PROJECT_ROOT_DIR}/scripts"
PROJECT_LIBRARIES_DIR="${PROJECT_ROOT_DIR}/libraries"
PROJECT_SOURCE_DIR="${PROJECT_ROOT_DIR}/src"

PROJECT_INO_SCRIPT="${PROJECT_SOURCE_DIR}/${PROJECT_NAME}.ino"

# Arduino dependencies
ARDUINO_LIBRARIES_SEARCH="arduino/libraries"
ARDUINO_LIBRARIES_DIR=$(locate "${ARDUINO_LIBRARIES_SEARCH}" -wil1)

if [ $? -ne 0 ] || [ ${#ARDUINO_LIBRARIES_DIR} -eq 0 ]; then
  echo "Arduino libraries directory could not be found!"
  exit 1;
fi

echo "Found Arduino libraries directory at path: '${ARDUINO_LIBRARIES_DIR}'"

ln -sft "${ARDUINO_LIBRARIES_DIR}" "${PROJECT_LIBRARIES_DIR}/"*

if [ $? -ne 0 ]; then
  echo "Libraries links could not be created!"
  exit 1;
fi

echo "Created symbolic links to all of the local project libraries in the Arduino libraries directory."

echo "Prebuild script done."
exit 0
